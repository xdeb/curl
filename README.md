## curl

HTTP/3 and QUIC ([EXPERIMENTAL](https://github.com/curl/curl/blob/master/docs/HTTP3.md)) enabled [cURL](https://curl.se) packaging, which products the latest version of cURL in `.deb` format. Since this is the _latest version_, you are running on the cutting edge if you choose to install.
